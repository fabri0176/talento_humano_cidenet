const Listado = {
    init() {
        const TABLA_EMPLEADOS = $('#tablaEmleados').DataTable({
            ajax: '/api/empleados/',
            type: 'GET',
            "columns": [
                { "data": "primer_apellido" },
                { "data": "segundo_apellido" },
                { "data": "primer_nombre" },
                { "data": "otros_nombres" },

                { "data": "pais_empleo" },
                { "data": "tipo_identificacion" },
                { "data": "numero_identificacion" },

                { "data": "email" },
                { "data": "fecha_ingreso" },
                { "data": "area" },
                { "data": "fecha_registro" },
                { "data": "fecha_actualizacion" },
                {
                    "data": null,
                    "render": function ( data, type, full, meta ) {
                        var buttonID = "rollover_"+full.id;
                        return `<a id='+buttonID+' class="btn btn-success rolloverBtn" role="button" onclick="window.location.href = 'editar/${full.id}'">Editar</a>`;
                    }
                }

            ]
        });
    }
};



$(document).ready(function () {
    Listado.init();
});