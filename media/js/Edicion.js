const Edicion = {
    id: undefined,
    method: undefined,
    url: '',
    data: {},
    list: [],
    save() {

        this.loadDataSave();
        $.ajax({
            type: this.method,
            url: this.url,
            contentType: 'application/json',
            data: JSON.stringify(this.data), // access in body
        }).done(function () {
            Edicion.redirect();
        }).fail(function (msg) {
            alert(JSON.stringify(msg));
        }).always(function (msg) {
            console.log('ALWAYS');
        });

    },
    loadDataSave() {
        let data = {
            "primer_apellido": ($('#primer_apellido').val()).trim(),
            "segundo_apellido": ($('#segundo_apellido').val()).trim(),
            "primer_nombre": ($('#primer_nombre').val()).trim(),
            "otros_nombres": ($('#otros_nombres').val()).trim(),
            "pais_empleo": $('#pais_empleo').val(),
            "tipo_identificacion": $('#tipo_identificacion').val(),
            "numero_identificacion": ($('#numero_identificacion').val()).trim(),
            "fecha_ingreso": $('#fecha_ingreso').val(),
            "area": $('#area').val(),
        };

        if (this.id == '') {
            this.method = 'POST';
            this.url = '/api/empleados/'
            this.data = data;
            return;
        }

        this.url = `/api/empleados/${this.id}`
        this.method = 'PUT';
        this.data = data;

    },
    registerEvent() {
        $("#btnGuardar").click(function () {
            Edicion.save();
        });

        $("#btnCancelar").click(function () {
            Edicion.redirect();
        });
    },
    redirect() {
        window.location.href = "/";
    },
    loadDataForm() {
        if (this.id == '') {
            return;
        }

        $.get(`/api/empleados/${this.id}`, function (data) {
            Edicion.setDataForm(data);
        });
    },
    setDataForm(data) {
        $('#primer_apellido').val(data['primer_apellido']);
        $('#segundo_apellido').val(data['segundo_apellido']);
        $('#primer_nombre').val(data['primer_nombre']);
        $('#otros_nombres').val(data['otros_nombres']);
        $('#pais_empleo').val(data['pais_empleo']);
        $('#tipo_identificacion').val(data['tipo_identificacion']);
        $('#numero_identificacion').val(data['numero_identificacion']);
        $('#fecha_ingreso').val(data['fecha_ingreso']);
        $('#area').val(data['area']);
        $('#id').val(data['id']);
    },
    loadLists() {
        $.get("/api/listas/", function (data) {
            this.list = data;

            for ([value, text] of Object.entries(data['AREA'])) {
                $('#area').append($('<option>', {
                    'value': value,
                    'text': text
                }));
            }

            for ([value, text] of Object.entries(data['TIPO_DOCUMENTO'])) {
                $('#tipo_identificacion').append($('<option>', {
                    'value': value,
                    'text': text
                }));
            }

            for ([value, text] of Object.entries(data['PAIS_EMPLEO'])) {
                $('#pais_empleo').append($('<option>', {
                    'value': value,
                    'text': text
                }));
            }

        });
    },

    init(recordId) {
        this.id = recordId;
        this.registerEvent();
        this.loadLists();
        this.loadDataForm();
    }
}