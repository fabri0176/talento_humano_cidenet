<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.2/datatables.css" />

    <title>ADMINISTRACION EMPLEADOS | NFP</title>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col">

            </div>
            <div class="col">
                <h1 class="text-center">ADMINISTRACION EMPLEADOS</h1>
            </div>
            <div class="col">

            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary" onclick="window.location.href = 'crear'">CREAR EMPLEADO</button>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col">
                <table id="tablaEmleados" class="table table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Primer Apellido</th>
                            <th>Segundo Apellido</th>
                            <th>Primer Nombre</th>
                            <th>Otros Nombres</th>
                            <th>Pais del Empleo</th>
                            <th>Tipo Identificación</th>
                            <th>Número Identificación</th>
                            <th>Correo Electrónico</th>
                            <th>Fecha Ingreso</th>
                            <th>Area</th>
                            <th>Fecha Registro</th>
                            <th>Fecha Actualización</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Primer Apellido</th>
                            <th>Segundo Apellido</th>
                            <th>Primer Nombre</th>
                            <th>Otros Nombres</th>
                            <th>Pais del Empleo</th>
                            <th>Tipo Identificación</th>
                            <th>Número Identificación</th>
                            <th>Correo Electrónico</th>
                            <th>Fecha Ingreso</th>
                            <th>Area</th>
                            <th>Fecha Registro</th>
                            <th>Fecha Actualización</th>
                            <th>Acción</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.2/datatables.js"></script>
    <script type="text/javascript" src="media/js/Listado.js"></script>



</body>

</html>