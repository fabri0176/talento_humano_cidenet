<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.2/datatables.css" />

    <title>ADMINISTRACION EMPLEADOS | NFP</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col">

            </div>
            <div class="col">
                <h1 class="text-center">ADMINISTRACION EMPLEADOS</h1>
            </div>
            <div class="col">

            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col">
                <div class="mb-3 row">
                    <label for="primer_apellido" class="col-sm-2 col-form-label">Primer Apellido</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="primer_apellido" value="">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="segundo_apellido" class="col-sm-2 col-form-label">Segundo Apellido</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="segundo_apellido" value="">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="primer_nombre" class="col-sm-2 col-form-label">Primer Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="primer_nombre" value="">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="otros_nombres" class="col-sm-2 col-form-label">Otros Nombres</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="otros_nombres" value="">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="pais_empleo" class="col-sm-2 col-form-label">Pais Empleo</label>
                    <div class="col-sm-10">
                        <select class="form-select" aria-label="Default select example" id="pais_empleo">
                        </select>
                    </div>
                </div>

                <div class="mb-3 row">
                    <label for="tipo_identificacion" class="col-sm-2 col-form-label">Tipo de Identificación</label>
                    <div class="col-sm-10">

                        <select class="form-select" aria-label="Default select example" id="tipo_identificacion">
                        </select>
                    </div>
                </div>

                <div class="mb-3 row">
                    <label for="numero_identificacion" class="col-sm-2 col-form-label">Número de Identificación</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="numero_identificacion" value="">
                    </div>
                </div>

                <div class="mb-3 row">
                    <label for="fecha_ingreso" class="col-sm-2 col-form-label">Fecha de ingreso</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="fecha_ingreso">
                        <input type="hidden" class="form-control" id="id">
                    </div>
                </div>

                <div class="mb-3 row">
                    <label for="area" class="col-sm-2 col-form-label">Area</label>
                    <div class="col-sm-10">
                        <select class="form-select" aria-label="Default select example" id="area">
                        </select>
                    </div>
                </div>

                <div class="mb-3 row">
                    <div class="col-sm-10">
                        <button type="button" class="btn btn-primary" id="btnGuardar">GUARDAR</button>
                        <button type="button" class="btn btn-light" id="btnCancelar">CANCELAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="/media/js/Edicion.js"></script>

    <script>
        const recordId = '<?php echo $id ?>';
        $(document).ready(function() {
            Edicion.init(recordId);
        });
    </script>


</body>

</html>