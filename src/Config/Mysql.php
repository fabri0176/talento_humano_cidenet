<?php

namespace App\Config;

use App\Config\DBConnectionInterface;
use PDO;

class Mysql implements DBConnectionInterface
{
    protected $db = null;

    public function __construct()
    {
        $DB = $GLOBALS['globalConfig']['DB'];
        $this->connect($DB['host'], $DB['user_name'], $DB['password'], $DB['db_name'], $DB['port']);
    }

    public function connect($host = '', $user = '', $pass = '', $dbName = '', $port = '')
    {
        
        $mysqlConnect = "mysql:host={$host};dbname={$dbName};port={$port}";
        $this->db = new PDO($mysqlConnect, $user, $pass);
    }

    public function query($query)
    {
        return $this->db->query($query);
    }

    public function getDb()
    {
        return $this->db;
    }
}
