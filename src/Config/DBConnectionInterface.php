<?php

namespace App\Config;

interface DBConnectionInterface
{
    public function connect($host = '', $user = '', $pass = '', $dbName = '', $port = '');
    public function query($query);
}
