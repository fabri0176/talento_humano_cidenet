<?php

namespace App\Libs\Empleados;

class Validador
{
    public static function numeroIdentificacion($numeroIdentificacion)
    {
        if (preg_match("/^[a-zA-Z0-9]{1,20}$/", $numeroIdentificacion)) {

            return true;
        }

        return false;
    }


    public static function apellidosNombre($texto)
    {
        if (preg_match("/^[A-Z]{1,20}$/", $texto)) {

            return true;
        }

        return false;
    }

    public static function otrosNombres($texto)
    {
        if (preg_match("/^[A-Z\ ]{1,50}$/", $texto)) {

            return true;
        }

        return false;
    }
}
