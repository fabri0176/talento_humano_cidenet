<?php

namespace App\Libs\Empleados;

use Exception;


class GeneradorEmail
{
    public static function generarEmail($primerNombre, $primerApellido, $dominio)
    {
        if (empty($primerNombre) || empty($primerApellido)) {
            throw new Exception('Datos para generar email no validos');
        }

        if (empty($dominio)) {
            throw new Exception('Dominio para calcular generar correo, no valido');
        }

        $primerApellido = self::limpiarCadenas($primerApellido);
        $primerNombre = self::limpiarCadenas($primerNombre);

        $email = sprintf("%s.%s@%s", $primerNombre, $primerApellido, $dominio);
        return $email;
    }

    private static function limpiarCadenas($string)
    {
        $string = mb_strtolower(trim($string));
        $string = str_replace(' ', '', $string);
        return $string;
    }
}
