<?php

namespace App\Models;

use App\Models\ModelInterface;
use App\Config\Mysql;
use PDO;
use App\Libs\Empleados\GeneradorEmail;
use App\Libs\Empleados\Validador;
use Exception;


class EmpleadosModel implements ModelInterface
{
    const TABLE = 'empleados';
    public $id;
    public $primer_apellido;
    public $segundo_apellido;
    public $primer_nombre;
    public $otros_nombres;
    public $pais_empleo;
    public $tipo_identificacion;
    public $numero_identificacion;
    public $email;
    public $fecha_ingreso;
    public $area;
    public $fecha_registro;


    /**
     * Undocumented function
     *
     * @param string $order
     * @param string $where
     * @return array
     */
    public static function getAll($order = "", $where = "", $limit = -1)
    {
        $result = [];
        $table = self::TABLE;
        $limit = intval($limit) > 0 ? "LIMIT {$limit}" : "";

        if (!empty($where)) {
            $where = "WHERE {$where}";
        }


        $sql = <<<SQL
        SELECT *
        FROM {$table}
        {$where}
        {$limit}
SQL;

        try {
            $mysql = new Mysql();
            $db = $mysql->getDb();
            $resultado = $db->query($sql);

            if ($resultado->rowCount() > 0) {
                $empleados = $resultado->fetchAll(PDO::FETCH_OBJ);

                foreach ($empleados as $empleado) {
                    $empleadoResult = new EmpleadosModel();
                    $empleado->id = $empleado->id;
                    $empleado->primer_apellido = $empleado->primer_apellido;
                    $empleado->segundo_apellido = $empleado->segundo_apellido;
                    $empleado->primer_nombre = $empleado->primer_nombre;
                    $empleado->otros_nombres = $empleado->otros_nombres;
                    $empleado->pais_empleo = $empleado->pais_empleo;
                    $empleado->tipo_identificacion = $empleado->tipo_identificacion;
                    $empleado->numero_identificacion = $empleado->numero_identificacion;
                    $empleado->email = $empleado->email;
                    $empleado->fecha_ingreso = $empleado->fecha_ingreso;
                    $empleado->area = $empleado->area;
                    $empleado->fecha_registro = $empleado->fecha_registro;

                    $result[] = $empleado;
                }
            }
            $resultado = null;
            $db = null;
        } catch (PDOException $e) {
            echo '{"error" : {"text":' . $e->getMessage() . '}';
        }

        return $result;
    }
    public function retrieve($id)
    {
        $empleados = self::getAll("", "id = '{$id}'", 1);
        if (count($empleados) > 0) {
            foreach (current($empleados) as $key =>  $value) {
                $this->$key = $value;
            }
        }
    }

    public function save()
    {

        $this->validar();

        if (empty($this->id)) {
            $this->_save();
            return true;
        }

        $this->_update();
        return true;
    }



    private function _save()
    {

        $sql = "INSERT INTO empleados (primer_apellido, segundo_apellido, primer_nombre, otros_nombres, pais_empleo, tipo_identificacion, numero_identificacion, email, fecha_ingreso, area) VALUES 
          (:primer_apellido, :segundo_apellido, :primer_nombre, :otros_nombres, :pais_empleo, :tipo_identificacion, :numero_identificacion, :email, :fecha_ingreso, :area)";

        $mysql = new Mysql();
        $db = $mysql->getDb();
        $resultado = $db->prepare($sql);

        $resultado->bindParam(':primer_apellido', $this->primer_apellido);
        $resultado->bindParam(':segundo_apellido', $this->segundo_apellido);
        $resultado->bindParam(':primer_nombre', $this->primer_nombre);
        $resultado->bindParam(':otros_nombres', $this->otros_nombres);
        $resultado->bindParam(':pais_empleo', $this->pais_empleo);
        $resultado->bindParam(':tipo_identificacion', $this->tipo_identificacion);
        $resultado->bindParam(':numero_identificacion', $this->numero_identificacion);
        $resultado->bindParam(':email', $this->email);
        $resultado->bindParam(':fecha_ingreso', $this->fecha_ingreso);
        $resultado->bindParam(':area', $this->area);

        if(!$resultado->execute()){
            throw new Exception(implode(' ',$resultado->errorInfo()));
        }

        $resultado = null;
        $db = null;
    }

    private function _update()
    {
        $sql = "UPDATE empleados 
        SET primer_apellido = :primer_apellido,
            segundo_apellido = :segundo_apellido,
            primer_nombre = :primer_nombre,
            otros_nombres = :otros_nombres,
            pais_empleo = :pais_empleo,
            tipo_identificacion = :tipo_identificacion,
            numero_identificacion = :numero_identificacion,
            email = :email,
            fecha_ingreso = :fecha_ingreso,
            area = :area
            WHERE empleados.id = {$this->id}";

        $mysql = new Mysql();
        $db = $mysql->getDb();
        $resultado = $db->prepare($sql);


        $resultado->bindParam(':primer_apellido', $this->primer_apellido);
        $resultado->bindParam(':segundo_apellido', $this->segundo_apellido);
        $resultado->bindParam(':primer_nombre', $this->primer_nombre);
        $resultado->bindParam(':otros_nombres', $this->otros_nombres);
        $resultado->bindParam(':pais_empleo', $this->pais_empleo);
        $resultado->bindParam(':tipo_identificacion', $this->tipo_identificacion);
        $resultado->bindParam(':numero_identificacion', $this->numero_identificacion);
        $resultado->bindParam(':email', $this->email);
        $resultado->bindParam(':fecha_ingreso', $this->fecha_ingreso);
        $resultado->bindParam(':area', $this->area);

        if(!$resultado->execute()){
            throw new Exception(implode(' ',$resultado->errorInfo()));
        }

        $resultado = null;
        $db = null;
    }

    public function retrieveByFieldString(array $params)
    {
    }

    public function generarEmail()
    {

        $email = GeneradorEmail::generarEmail($this->primer_nombre, $this->primer_apellido, $GLOBALS['globalConfig']['EMAILS']['DOMINIO']);

        $exist = false;
        $itter = 1;
        do {
            $exist = self::existEmail($email);

            if ($exist == true) {
                $email = GeneradorEmail::generarEmail($this->primer_nombre, $this->primer_apellido . $itter, $GLOBALS['globalConfig']['EMAILS']['DOMINIO']);
            }
            $itter++;
        } while ($exist == true);

        return $email;
    }


    public static function existEmail($email)
    {
        $table = self::TABLE;
        $sql = <<<SQL
        SELECT *
        FROM {$table}
        WHERE {$table}.email = '{$email}'
SQL;

        $mysql = new Mysql();
        $db = $mysql->getDb();
        $resultado = $db->query($sql);

        if ($resultado->rowCount() > 0) {
            return true;
        }

        return false;
    }


    private function validar()
    {
        if (!Validador::apellidosNombre($this->primer_nombre)) {
            throw new Exception('Primer nombre no valido');
        }
        if (!Validador::apellidosNombre($this->primer_apellido)) {
            throw new Exception('Primer apellido no valido');
        }
        if (!Validador::apellidosNombre($this->segundo_apellido)) {
            throw new Exception('Segundo apellido no valido');
        }

        if (!Validador::otrosNombres($this->otros_nombres)) {
            throw new Exception('Otros nombres no valido');
        }

        if (!Validador::numeroIdentificacion($this->numero_identificacion)) {
            throw new Exception('Número de identificación no valido');
        }
    }
}
