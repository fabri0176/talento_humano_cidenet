<?php

namespace App\Models;

interface ModelInterface
{

    public static function getAll(string $order, string $where, int $limit);
    public function retrieve(int $id);
    public function save();
    public function retrieveByFieldString(array $params);
}
