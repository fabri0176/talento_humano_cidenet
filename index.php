<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use App\Models\EmpleadosModel;
use App\Middleware\JsonBodyParserMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use phpDocumentor\Reflection\PseudoTypes\True_;
use Slim\Views\PhpRenderer;

require 'vendor/autoload.php';

$app = AppFactory::create();



$app->get('/', function (Request $request, Response $response, $args) {
    $renderer = new PhpRenderer('templates');
    return $renderer->render($response, "index.php", $args);
});

$app->get('/crear', function (Request $request, Response $response, $args) {
    $renderer = new PhpRenderer('templates');
    $args['id'] = null;
    return $renderer->render($response, "crearEditar.php", $args);
});

$app->get('/editar/{id}', function (Request $request, Response $response, $args) {
    $renderer = new PhpRenderer('templates');
    return $renderer->render($response, "crearEditar.php", $args);
});


$app->get('/api/empleados/', function (Request $request, Response $response, $args) {
    $empleados = EmpleadosModel::getAll();
    $payload = json_encode(['data' => $empleados]);
    $response->getBody()->write($payload);
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(201);
});

$app->get('/api/empleados/{id}', function (Request $request, Response $response, $args) {
    $id = $args['id'];
    $empleado = new EmpleadosModel();
    $empleado->retrieve($id);
    $payload = json_encode($empleado);
    $response->getBody()->write($payload);

    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(201);
});

$app->post('/api/empleados/',  function (Request $request, Response $response) {

    $logger = new Logger('empleados_post');
    $logger->pushHandler(new StreamHandler('logs/api/post/log_post_' . date('Ymd') . '.log', Logger::DEBUG));
    $params = $request->getParsedBody();
    $logger->info('Datos empleados antes de guardar', $params);

    try {
        $empleado = new EmpleadosModel();
        $empleado->primer_apellido = $params['primer_apellido'];
        $empleado->segundo_apellido = $params['segundo_apellido'];
        $empleado->primer_nombre = $params['primer_nombre'];
        $empleado->otros_nombres = $params['otros_nombres'];
        $empleado->pais_empleo = $params['pais_empleo'];
        $empleado->tipo_identificacion = $params['tipo_identificacion'];
        $empleado->numero_identificacion = $params['numero_identificacion'];
        $empleado->fecha_ingreso = $params['fecha_ingreso'];
        $empleado->area = $params['area'];
        $empleado->email = $empleado->generarEmail();
        $empleado->save();
        $payload = json_encode($empleado);
        $response->getBody()->write($payload);
        $status = 201;
        $logger->info('Datos empleados despues de guardar', (array)$empleado);
    } catch (Exception $e) {
        $status = 400;
        $logger->error($e->getMessage());

        $response->getBody()->write(json_encode(['error' => $e->getMessage()]));
    }

    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus($status);
})->add(new JsonBodyParserMiddleware());



$app->put('/api/empleados/{id}',  function (Request $request, Response $response, $args) {
    $id = $args['id'];
    $logger = new Logger('empleados_post');
    $logger->pushHandler(new StreamHandler('logs/api/put/log_put_' . date('Ymd') . '.log', Logger::DEBUG));
    $params = $request->getParsedBody();
    $logger->info('Datos empleados antes de guardar', $params);
    $status = 400;
    try {
        $empleado = new EmpleadosModel();
        $empleado->retrieve($id);

        if (!empty($empleado->id)) {
            $newEmail = false;

            if ($empleado->primer_apellido != $params['primer_apellido'] || $empleado->primer_nombre != $params['primer_nombre']) {
                $newEmail = true;
            }

            $empleado->primer_apellido = $params['primer_apellido'];
            $empleado->segundo_apellido = $params['segundo_apellido'];
            $empleado->primer_nombre = $params['primer_nombre'];
            $empleado->otros_nombres = $params['otros_nombres'];
            $empleado->pais_empleo = $params['pais_empleo'];
            $empleado->tipo_identificacion = $params['tipo_identificacion'];
            $empleado->numero_identificacion = $params['numero_identificacion'];
            $empleado->fecha_ingreso = $empleado->fecha_ingreso;
            $empleado->area = $params['area'];

            $empleado->email = $newEmail == false ? $empleado->email : $empleado->generarEmail();

            $empleado->save();
            $payload = json_encode($empleado);
            $response->getBody()->write($payload);
            $status = 201;
            $logger->info('Datos empleados despues de guardar', (array)$empleado);
        }
    } catch (Exception $e) {
        $status = 400;
        $logger->error($e->getMessage());
        $response->getBody()->write(json_encode(['error' => $e->getMessage()]));
    }

    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus($status);
})->add(new JsonBodyParserMiddleware());

$app->get('/api/listas/', function (Request $request, Response $response, $args) {

    $payload = json_encode($GLOBALS['listas']);
    $response->getBody()->write($payload);
    return $response
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(201);
});


$app->run();
