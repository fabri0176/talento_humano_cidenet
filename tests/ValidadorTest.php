<?php



use PHPUnit\Framework\TestCase;
use App\Libs\Empleados\Validador;

class ValidadorTest extends TestCase
{

    protected function setUp(): void
    {

        parent::setUp();
    }

    public function testNumeroIdentificacion()
    {

        $this->assertFalse(Validador::numeroIdentificacion("123456789ALSDDSDASDXXXXLLLLLL"));

        $this->assertTrue(Validador::numeroIdentificacion("123456789ALSDDSD"));

        $this->assertFalse(Validador::numeroIdentificacion("3324234324ññÑÑ234"));

        $this->assertTrue(Validador::numeroIdentificacion("aAX01232332"));
        $this->assertFalse(Validador::numeroIdentificacion("aAX01232332--33"));
    }


    public function testApellidosNombre()
    {
        $this->assertTrue(Validador::apellidosNombre('PARRA'));
        $this->assertFalse(Validador::apellidosNombre('Parra'));
        $this->assertTrue(Validador::apellidosNombre('GONZALEZ'));
        $this->assertFalse(Validador::apellidosNombre('González'));
        $this->assertFalse(Validador::apellidosNombre('González253'));
        $this->assertFalse(Validador::apellidosNombre('GONZALEZGONZALEZGONZALEZGONZALEZGONZALEZGONZALEZ '));
        $this->assertFalse(Validador::apellidosNombre('TOÑO'));


        $this->assertFalse(Validador::apellidosNombre('GONZALEZ  ', 20));
    }

    public function testOtrosNombres()
    {
        $this->assertTrue(Validador::otrosNombres('LEONARDO ALZATE'));
        $this->assertFalse(Validador::otrosNombres('LEONARDO_ALZATE'));
        $this->assertTrue(Validador::otrosNombres('GONZALEZ'));
        $this->assertFalse(Validador::otrosNombres('González'));
        $this->assertFalse(Validador::otrosNombres('González253'));
        $this->assertFalse(Validador::otrosNombres('LEONARDO ALZATELEONARDO ALZATELEONARDO ALZATELEONARDO ALZATELEONARDO ALZATELEONARDO ALZATE'));
    }
}
