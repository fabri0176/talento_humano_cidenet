<?php



use PHPUnit\Framework\TestCase;
use App\Libs\Empleados\GeneradorEmail;

class GeneradorEmailTest extends TestCase
{

    protected function setUp(): void
    {

        parent::setUp();
    }

    public function testGeneradorEmail()
    {

        $this->assertEquals('juan.perez@cidenet.com.co', GeneradorEmail::generarEmail('JUAN', 'PEREZ', 'cidenet.com.co'));
    }

    public function testGeneradorEmailNombreCompuesto()
    {

        $this->assertEquals('juan.delacalle@cidenet.com.co', GeneradorEmail::generarEmail('JUAN', 'DE LA CALLE','cidenet.com.co'));
    }
}
