<?php

$GLOBALS['listas'] = [
    'AREA' => [
        '' => '',
        'ADMINISTRACION' => 'Administración',
        'FINANCIERA' => 'Financiera',
        'COMPRAS' => 'Compras',
        'INFRAESTRUCTURA' => 'Infraestructura',
        'OPERACION' => 'Operación',
        'TALENTO_HUMANO' => 'Talento Humano',
        'SERVICIOS_VARIOS' => 'Servicios Varios',
    ],
    'TIPO_DOCUMENTO' => [
        '' => '',
        'CEDULA_DE_CIUDADANIA' => 'Cédula de Ciudadanía',
        'CEDULA_DE_EXTRANJERIA' => 'Cédula de Extranjería',
        'PASAPORTE' => 'Pasaporte',
        'PERMISO_ESPECIAL' => 'Permiso Especial',
    ],
    'PAIS_EMPLEO' => [
        '' => '',
        'COLOMBIA' => 'Colombia',
        'ESTADOS_UNIDOS' => 'Estados Unidos',
    ],
];
